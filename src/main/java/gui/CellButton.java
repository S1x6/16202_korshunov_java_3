package gui;

import javax.swing.*;
import java.awt.*;
import java.net.MalformedURLException;
import java.net.URISyntaxException;

public class CellButton extends JButton {

    public enum ButtonState {
        CLOSED,
        REVEALED,
        MINE,
        FLAG
    }

    private int posX;
    private int posY;

    CellButton() {
        super();
        setPreferredSize(new Dimension(32, 32));
        setHorizontalTextPosition(JButton.CENTER);
        setVerticalTextPosition(JButton.CENTER);
        setBackgroundImage(CellButton.ButtonState.CLOSED);
        setMargin(new Insets(0, 0, 0, 0));
    }

    public void setBackgroundImage(ButtonState state) {
        switch (state) {
            case CLOSED:
            default:
                try {
                    setIcon(new ImageIcon(getClass().getResource("/drawable/clr.png").toURI().toURL()));
                } catch (MalformedURLException | URISyntaxException e) {
                    e.printStackTrace();
                }
                break;
            case REVEALED:
                try {
                    setIcon(new ImageIcon(getClass().getResource("/drawable/opened.png").toURI().toURL()));
                } catch (MalformedURLException | URISyntaxException e) {
                    e.printStackTrace();
                }
                break;
            case MINE:
                try {
                    setIcon(new ImageIcon(getClass().getResource("/drawable/mine.png").toURI().toURL()));
                } catch (MalformedURLException | URISyntaxException e) {
                    e.printStackTrace();
                }
                break;
            case FLAG:
                try {
                    setIcon(new ImageIcon(getClass().getResource("/drawable/flag.png").toURI().toURL()));
                } catch (MalformedURLException | URISyntaxException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }
}
