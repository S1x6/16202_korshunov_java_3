package gui;

import javax.swing.*;
import java.awt.*;

class NameDialog extends JDialog {
    NameDialog(Window window) {
        super(window);
        this.setBounds(100,100,200,100);
        JLabel label = new JLabel("Введите имя игрока");
        JButton button = new JButton("Сохранить");
        JTextField field = new JTextField();
        setLayout(new BorderLayout());
        add(label, BorderLayout.NORTH);
        add(field, BorderLayout.CENTER);
        add(button, BorderLayout.SOUTH);
        button.addActionListener(e -> {
            ((GameFrame)getParent()).setNameForRecords(field.getText());
            this.setVisible(false);
        });
    }

}
