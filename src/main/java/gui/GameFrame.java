package gui;

import model.Cell;
import model.Model;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowListener;
import java.io.IOException;

public class GameFrame extends JFrame {

    private Model model;
    private JMenuBar menuBar;
    private JMenu gameMenu;
    private JMenuItem newGameMenuItem;
    private JMenuItem exitMenuItem;
    private JPanel topPanel;
    private JPanel fieldPanel;
    private JRadioButtonMenuItem easyMenuItem;
    private JRadioButtonMenuItem normalMenuItem;
    private JRadioButtonMenuItem hardMenuItem;
    private JLabel timeLabel;
    private JLabel flagsLabel;
    private CellButton[][] cells;
    private String recordName;

    public GameFrame(Model model) {
        super("Minesweeper");
        this.model = model;
        resetView(200, 200);
        initializeMenu();
        updateState();
        model.setOnTimeUpdatedListener(() -> {
            if (!model.isStopped()) {
                timeLabel.setText(model.getTime());
            }
        });
    }

    private void resetView(int x, int y) {
        this.setBounds(x, y, model.getWidth() * 32, model.getHeight() * 32 + 80);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        if (fieldPanel != null)
            this.remove(fieldPanel);
        cells = new CellButton[model.getWidth()][model.getWidth()];
        this.setLayout(new BorderLayout());
        topPanel = new JPanel();
        timeLabel = new JLabel();
        flagsLabel = new JLabel();
        topPanel.setLayout(new BorderLayout());
        topPanel.add(timeLabel, BorderLayout.WEST);
        topPanel.add(flagsLabel, BorderLayout.EAST);
        this.add(topPanel, BorderLayout.NORTH);
        fieldPanel = new JPanel();
        fieldPanel.setLayout(new GridLayout(model.getWidth(), model.getHeight()));
        for (int i = 0; i < model.getWidth(); i++) {
            for (int j = 0; j < model.getWidth(); j++) {
                CellButton b = new CellButton();
                b.setPosX(i);
                b.setPosY(j);
                b.addMouseListener(new MouseListener() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if (SwingUtilities.isRightMouseButton(e)) {
                            model.setFlag(b.getPosX(), b.getPosY());
                            updateState();
                        }
                    }

                    @Override
                    public void mousePressed(MouseEvent e) {

                    }

                    @Override
                    public void mouseReleased(MouseEvent e) {

                    }

                    @Override
                    public void mouseEntered(MouseEvent e) {

                    }

                    @Override
                    public void mouseExited(MouseEvent e) {

                    }
                });
                b.addActionListener(e -> {
                    switch (model.revealAt(b.getPosX(), b.getPosY())) {
                        case 0:
                            this.updateState();
                            break;
                        case -1:
                            JOptionPane.showMessageDialog(this, "Game over!");
                            model.revealAll();
                            updateState();
                            break;
                        case 1:
                            model.revealAll();
                            updateState();
                            JOptionPane.showMessageDialog(this, "Congratulations! You won!");
                            NameDialog name = new NameDialog(this);
                            name.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                            name.setModal(true);
                            name.setVisible(true);
                            try {
                                model.saveRecords(Model.DEFAULT_RECORDS_FILE_NAME, recordName);
                            } catch (IOException e1) {
                                e1.printStackTrace();
                                JOptionPane.showMessageDialog(this, "Не удалось сохранить рекорд");
                            }
                            break;
                    }
                });
                cells[i][j] = b;
                fieldPanel.add(b);
            }
        }

        this.add(fieldPanel, BorderLayout.CENTER);
        fieldPanel.revalidate();
        fieldPanel.repaint();
        this.revalidate();
        this.repaint();
    }

    void setNameForRecords(String name) {
        recordName = name;
    }

    private void updateState() {
        flagsLabel.setText("Flags: " + model.getFlags());
        timeLabel.setText(model.getTime());
        for (int i = 0; i < model.getWidth(); i++) {
            for (int j = 0; j < model.getHeight(); j++) {
                Cell c = model.getCell(i, j);
                CellButton b = cells[i][j];
                if (c.isRevealed()) {
                    if (!c.isMine()) {
                        b.setBackgroundImage(CellButton.ButtonState.REVEALED);
                        b.setText(c.getMinesNearby() == 0 ? "" : String.valueOf(c.getMinesNearby()));
                    } else {
                        b.setBackgroundImage(CellButton.ButtonState.MINE);
                    }
                } else if (c.isHasFlag()) {
                    b.setBackgroundImage(CellButton.ButtonState.FLAG);
                } else {
                    b.setBackgroundImage(CellButton.ButtonState.CLOSED);
                    b.setText("");
                }
            }
        }
    }

    private void initializeMenu() {
        menuBar = new JMenuBar();
        gameMenu = new JMenu("Game");
        newGameMenuItem = new JMenuItem("New game");
        easyMenuItem = new JRadioButtonMenuItem("Easy");
        normalMenuItem = new JRadioButtonMenuItem("Normal");
        hardMenuItem = new JRadioButtonMenuItem("Hard");
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(easyMenuItem);
        buttonGroup.add(normalMenuItem);
        buttonGroup.add(hardMenuItem);
        newGameMenuItem.addActionListener(e -> {
            model.reset();
            this.resetField();
        });
        easyMenuItem.addActionListener(e -> {
            if (easyMenuItem.isSelected()) {
                model.setDifficulty(Model.GameDifficulty.EASY);
                resetView(200, 200);
                updateState();
            }
        });
        normalMenuItem.addActionListener(e -> {
            if (normalMenuItem.isSelected()) {
                model.setDifficulty(Model.GameDifficulty.MEDIUM);
                resetView(200, 200);
                updateState();
            }
        });
        hardMenuItem.addActionListener(e -> {
            if (hardMenuItem.isSelected()) {
                model.setDifficulty(Model.GameDifficulty.HARD);
                resetView(200, 200);
                updateState();
            }
        });
        exitMenuItem = new JMenuItem("Exit");
        exitMenuItem.addActionListener(e -> this.dispose());
        gameMenu.add(newGameMenuItem);
        gameMenu.add(easyMenuItem);
        gameMenu.add(normalMenuItem);
        gameMenu.add(hardMenuItem);
        gameMenu.add(exitMenuItem);
        menuBar.add(gameMenu);
        easyMenuItem.setSelected(true);
        this.setJMenuBar(menuBar);
    }

    private void resetField() {
        updateState();
    }
}
