package console_gui.commands;

import console_gui.exception.command.CommandException;
import console_gui.exception.command.WrongArgTypeException;
import console_gui.exception.command.WrongNumberArgException;
import model.Model;

public class CommandOpen extends Command {

    @Override
    public void execute(Model model) throws CommandException {
        if (getArgumentList().size() != 2) {
            throw new WrongNumberArgException("usage: open x{0-" + (model.getWidth() - 1) + "} y{0-" + (model.getHeight() - 1) + "}");
        }
        int x;
        int y;
        try {
            x = Integer.valueOf((String) getArgumentList().get(0));
            y = Integer.valueOf((String) getArgumentList().get(1));
        } catch (NumberFormatException ex) {
            throw new WrongArgTypeException("Arguments are not int");
        }
        if (x >= model.getWidth() || y >= model.getHeight()) {
            throw new CommandException("index is bigger field size");
        }
        model.revealAt(x, y);
    }

}
