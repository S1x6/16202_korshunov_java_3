package console_gui.commands;

import console_gui.exception.command.CommandException;
import console_gui.exception.command.WrongNumberArgException;
import model.Model;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CommandHighScores extends Command{
    @Override
    public void execute(Model model) throws CommandException {
        if (getArgumentList() != null) {
            throw new WrongNumberArgException("usage: highscores");
        }
        File f = new File(Model.DEFAULT_RECORDS_FILE_NAME);
        if (!f.exists()) {
            System.out.println("No records");
        }
        Scanner scanner;
        try {
            scanner = new Scanner(f);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }
        while (scanner.hasNextLine()) {
            System.out.println(scanner.nextLine());
        }
        System.out.println("Press enter");
        new Scanner(System.in).nextLine();
    }
}
