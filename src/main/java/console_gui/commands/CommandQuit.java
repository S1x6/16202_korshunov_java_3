package console_gui.commands;

import console_gui.exception.command.CommandException;
import console_gui.exception.command.WrongNumberArgException;
import model.Model;

import static java.lang.System.exit;

public class CommandQuit extends Command {
    @Override
    public void execute(Model model) throws CommandException {
        if (getArgumentList() != null) {
            throw new WrongNumberArgException("usage: quit");
        }
        exit(0);
    }
}
