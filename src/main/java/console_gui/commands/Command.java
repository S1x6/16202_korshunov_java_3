package console_gui.commands;

import console_gui.exception.command.CommandException;
import model.Model;

import java.util.ArrayList;

public abstract class Command {
    private ArrayList<Object> argumentList;

    public abstract void execute(Model model) throws CommandException;

    public final void setArgumentList(ArrayList<Object> argumentList) {
        this.argumentList = argumentList;
    }

    final ArrayList<Object> getArgumentList() {
        return argumentList;
    }
}
