package console_gui.commands;

import console_gui.exception.command.CommandException;
import console_gui.exception.command.WrongArgTypeException;
import console_gui.exception.command.WrongNumberArgException;
import model.Model;

public class CommandChangeDifficulty extends Command {
    @Override
    public void execute(Model model) throws CommandException {
        if (getArgumentList() == null || (getArgumentList().size() != 1 && getArgumentList().size() != 3)) {
            throw new WrongNumberArgException("usage: chdif width height mines OR chdif mode{1-3}");
        }
        System.out.println("хы");
        if (getArgumentList().size() == 1) {
            int x;
            try {
                x = Integer.valueOf((String) getArgumentList().get(0));
            } catch (NumberFormatException ex) {
                throw new WrongArgTypeException("Argument is not int");
            }
            if (x < 1 || x > 3) {
                throw new CommandException("Mode can be 1, 2 or 3");
            }
            model.setDifficulty(x == 1 ? Model.GameDifficulty.EASY : x == 2 ? Model.GameDifficulty.MEDIUM : Model.GameDifficulty.HARD);
        } else {
            int x;
            int y;
            int m;
            try {
                x = Integer.valueOf((String) getArgumentList().get(0));
                y = Integer.valueOf((String) getArgumentList().get(1));
                m = Integer.valueOf((String) getArgumentList().get(2));
            } catch (NumberFormatException ex) {
                throw new WrongArgTypeException("Arguments are not int");
            }
            if (x < 1 || y < 1 || m < 1) {
                throw new CommandException("Arguments should be positive");
            }
            if (m >= x * y) {
                throw new CommandException("Too many mines");
            }
            model.setAttributes(x, y, m);
        }
    }
}
