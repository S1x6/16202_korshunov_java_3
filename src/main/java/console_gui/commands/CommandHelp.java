package console_gui.commands;

import console_gui.exception.command.CommandException;
import console_gui.exception.command.WrongNumberArgException;
import model.Model;

import java.util.Scanner;

public class CommandHelp extends Command {
    @Override
    public void execute(Model model) throws CommandException {
        if (getArgumentList() != null) {
            throw new WrongNumberArgException("usage: help");
        }
        System.out.println("Available commands:");
        System.out.println("open x y - reveals the cell on x y field");
        System.out.println("flag x y - sets a flag on x y field, which prevents it from revealing, or removes the flag");
        System.out.println("quit (close game)");
        System.out.println("chdif mode ( 1 - easy, 2 - medium, 3 - hard ) - changes difficulty");
        System.out.println("chdif width height mines - changes difficulty");
        System.out.println("newgame - start a new game");
        System.out.println("highscores - shows record table");
        System.out.println();
        System.out.println("Press enter");
        new Scanner(System.in).nextLine();

    }
}
