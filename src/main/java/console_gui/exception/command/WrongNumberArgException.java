package console_gui.exception.command;

public class WrongNumberArgException extends CommandException {
    public WrongNumberArgException(String msg){
        super(msg);
    }
}
