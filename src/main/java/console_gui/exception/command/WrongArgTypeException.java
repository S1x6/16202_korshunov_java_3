package console_gui.exception.command;

public class WrongArgTypeException extends CommandException {
    public WrongArgTypeException(String msg) {
        super(msg);
    }
}
