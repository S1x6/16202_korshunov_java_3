package console_gui.exception.factory;

public class FactoryException extends Exception {
    public FactoryException(String msg) {
        super(msg);
    }
}
