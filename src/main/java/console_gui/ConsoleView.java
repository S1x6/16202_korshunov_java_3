package console_gui;

import console_gui.commands.Command;
import console_gui.exception.command.CommandException;
import console_gui.exception.factory.FactoryException;
import console_gui.factory.Factory;
import model.Cell;
import model.Model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class ConsoleView {

    private Model model;
    private Factory factory;
    private Scanner scanner = new Scanner(System.in);

    public ConsoleView(Model model) throws IOException, ClassNotFoundException {
        this.model = model;
        // не могу нормально сделать чтобы файл был виден и с джара и с идеи
        this.factory = new Factory(getClass().getResource("/ini/factory.ini").getFile());
    }

    private void redraw() {
        try {
            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < model.getHeight(); ++i) {
            for (int j = 0; j < model.getWidth(); ++j) {
                System.out.print(cellToString(model.getCell(i, j)));
            }
            System.out.println();
        }
    }

    private String cellToString(Cell c) {
        if (c.isRevealed()) {
            if (!c.isMine()) {
                return String.valueOf(c.getMinesNearby());
            } else {
                return "*";
            }
        } else if (c.isHasFlag()) {
            return "F";
        } else {
            return ".";
        }
    }

    public void startGame() {
        String message = null;
        while (true) {
            redraw();
            if (message != null) {
                System.out.println(message);
                message = null;
            }
            try {
                Command command = getNextCommand();
                if (command == null)
                    continue;

                command.execute(model);
                if (model.getGameStatus() == Model.GameStatus.FAIL) {
                    redraw();
                    System.out.println("GAME OVER!");
                    System.out.println("Press enter");
                    scanner.nextLine();
                    model.reset();
                } else if (model.getGameStatus() == Model.GameStatus.VICTORY) {
                    String s_name = "";
                    while (s_name.equals("")) {
                        redraw();
                        System.out.println("VICTORY!");
                        System.out.print("Please, enter your name: ");
                        s_name = scanner.nextLine();
                    }
                    try {
                        model.saveRecords(Model.DEFAULT_RECORDS_FILE_NAME, s_name);
                    } catch (IOException e) {
                        System.out.println("Record was not saved");
                    }
                    model.reset();
                    redraw();
                }
            } catch (CommandException | FactoryException e) {
                message = e.getMessage();
            }
        }
    }

    private Command getNextCommand() throws FactoryException {
        String input = scanner.nextLine();
        String[] tokens = input.trim().split("\\s");
        Command command = factory.getInstanceFor(tokens[0]);
        if (command == null)
            return null;
        if (tokens.length > 1) {
            command.setArgumentList(new ArrayList<>(Arrays.asList(input.substring(input.indexOf(' ') + 1).split("\\s"))));
        }
        return command;
    }
}
