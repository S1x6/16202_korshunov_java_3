package console_gui.factory;

import console_gui.commands.Command;
import console_gui.exception.factory.FactoryException;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

public class Factory {
    private HashMap<String, Class> map;

    public Factory(String initializationFileName) throws IOException, ClassNotFoundException {
        map = new HashMap<>(10);

        /*Scanner scanner = new Scanner(new File(initializationFileName));
        while (scanner.hasNext()) {
            String key = scanner.next();
            if (scanner.hasNext()) {
                map.put(key, Class.forName(scanner.next()));
            } else {
                throw new IOException("Wrong format of ini file");
            }
        }*/

        // временный костыль
        map.put("open", Class.forName("console_gui.commands.CommandOpen"));
        map.put("flag", Class.forName("console_gui.commands.CommandFlag"));
        map.put("newgame", Class.forName("console_gui.commands.CommandNewGame"));
        map.put("highscores", Class.forName("console_gui.commands.CommandHighScores"));
        map.put("quit", Class.forName("console_gui.commands.CommandQuit"));
        map.put("chdif", Class.forName("console_gui.commands.CommandChangeDifficulty"));
        map.put("help", Class.forName("console_gui.commands.CommandHelp"));
    }

    public Command getInstanceFor(String key) throws FactoryException{
        try {
            return (Command) map.get(key).newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            return null;
        } catch (NullPointerException ex) {
            throw new FactoryException("No such command");
        }
    }


}
