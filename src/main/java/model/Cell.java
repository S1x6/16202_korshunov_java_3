package model;

public class Cell {

    boolean isMine = false;
    int minesNearby = 0;
    boolean revealed = false;
    boolean hasFlag = false;

    Cell() {
    }

    Cell(Cell c) {
        isMine = c.isMine;
        minesNearby = c.minesNearby;
        revealed = c.revealed;
        hasFlag = c.hasFlag;
    }

    public void increaseMinesNearby() {
        ++minesNearby;
    }

    public void setHasFlag(boolean hasFlag) {
        this.hasFlag = hasFlag;
    }

    public boolean isHasFlag() {
        return hasFlag;
    }

    public void setRevealed(boolean revealed) {
        this.revealed = revealed;
    }

    public boolean isRevealed() {
        return revealed;
    }

    public void setMine(boolean mine) {
        isMine = mine;
    }

    public int getMinesNearby() {
        return minesNearby;
    }

    public boolean isMine() {
        return isMine;
    }

    public void setMinesNearby(int minesNearby) {
        this.minesNearby = minesNearby;
    }
}
