package model;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import static model.Model.GameStatus.FAIL;
import static model.Model.GameStatus.IN_PROCESS;
import static model.Model.GameStatus.VICTORY;

public class Model {

    public static String DEFAULT_RECORDS_FILE_NAME = "records.txt";

    private int height;
    private int width;
    private int mines;
    private int flags;
    private volatile boolean isStopped;
    private volatile AtomicInteger time = new AtomicInteger(0);
    private int currentTime = 0;
    private volatile OnTimeUpdatedListener onTimeUpdatedListener = null;
    private Cell[][] field;
    private int clearMinesCount;
    private GameDifficulty difficulty = GameDifficulty.CUSTOM;
    private GameStatus gameStatus = IN_PROCESS;


    public enum GameDifficulty {
        EASY,
        MEDIUM,
        HARD,
        CUSTOM
    }

    public enum GameStatus {
        VICTORY,
        FAIL,
        IN_PROCESS
    }

    public Model() {
        this(9, 9, 10);
    }

    public Model(GameDifficulty initialDifficulty) {
        setDifficulty(initialDifficulty);
        Thread timeThread = new Thread(() -> {
            try {
                while (true) {
                    Thread.sleep(1000);
                    if (!isStopped) {
                        time.incrementAndGet();
                        if (onTimeUpdatedListener != null)
                            onTimeUpdatedListener.performAction();
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        timeThread.setDaemon(true);
        timeThread.start();
    }

    public Model(int width, int height, int mines) {
        setAttributes(width, height, mines);
        Thread timeThread = new Thread(() -> {
            try {
                while (true) {
                    Thread.sleep(1000);
                    if (!isStopped) {
                        time.incrementAndGet();
                        if (onTimeUpdatedListener != null)
                            onTimeUpdatedListener.performAction();
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        timeThread.setDaemon(true);
        timeThread.start();
    }

    public void setAttributes(int width, int height, int mines) {
        this.width = width;
        this.height = height;
        this.mines = mines;
        if (width == 9 && height == 9 && mines == 10) {
            difficulty = GameDifficulty.EASY;
        } else if (width == 18 && height == 18 && mines == 40 ) {
            difficulty = GameDifficulty.MEDIUM;
        } else if (width == 24 && height == 24 && mines == 80) {
            difficulty = GameDifficulty.HARD;
        } else {
            difficulty = GameDifficulty.CUSTOM;
        }
        reset();
    }

    private void setMines() {
        int x, y;
        Random r = new Random();
        for (int i = 0; i < mines; ++i) {
            do {
                x = r.nextInt(width);
                y = r.nextInt(height);
            } while (field[x][y].isMine);
            field[x][y].setMine(true);
            performActionForNeighbours(x, y, (x1, y1) -> field[x1][y1].increaseMinesNearby());
        }
    }

    private void performActionForNeighbours(int x, int y, CellAction action) {
        if (x > 0) {
            action.performAction(x - 1, y);
            if (y > 0)
                action.performAction(x - 1, y - 1);
            if (y < height - 1)
                action.performAction(x - 1, y + 1);
        }
        if (x < width - 1) {
            action.performAction(x + 1, y);
            if (y > 0) {
                action.performAction(x + 1, y - 1);
            }
            if (y < height - 1)
                action.performAction(x + 1, y + 1);
        }
        if (y > 0)
            action.performAction(x, y - 1);
        if (y < height - 1)
            action.performAction(x, y + 1);
    }

    public void reset() {
        flags = mines;
        this.clearMinesCount = width * height - mines;
        field = new Cell[width][height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                field[i][j] = new Cell();
            }
        }
        setMines();
        currentTime = time.get();
        isStopped = false;
        gameStatus = GameStatus.IN_PROCESS;
    }

    public synchronized String getTime() {
        int sec = time.get() - currentTime;
        return (sec / 60 < 10 ? "0" + sec / 60 : sec / 60) + ":" + (sec % 60 < 10 ? "0" + sec % 60 : sec % 60);
    }

    public GameStatus getGameStatus() {
        return gameStatus;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public int getMines() {
        return mines;
    }

    public Cell getCell(int i, int j) {
        return new Cell(field[i][j]);
    }

    public void setDifficulty(GameDifficulty difficulty) {
        switch (difficulty) {
            case EASY:
            default:
                setAttributes(9,9,10);
                break;
            case MEDIUM:
                setAttributes(18,18,40);
                break;
            case HARD:
                setAttributes(24,24,80);
                break;
        }
    }

    public void saveRecords(String fileName, String gamerName) throws IOException {
        File f = new File(fileName);
        if (!f.exists()) {
            if (!f.createNewFile())
                throw new IOException("Could not create a file");
        }
        Files.write(Paths.get(fileName), ((gamerName == null || gamerName.equals("")? "NoName" : gamerName) + " " + difficulty.toString() + " " + getTime() + "\r\n").getBytes(), StandardOpenOption.APPEND);
    }

    /**
     * @param posX
     * @param posY
     * @return -1 if mine found, 1 in case of victory, 0 otherwise,
     */
    public int revealAt(int posX, int posY) {
        if (field[posX][posY].isHasFlag() || field[posX][posY].isRevealed()) {
            gameStatus = IN_PROCESS;
            return 0;
        }
        if (field[posX][posY].isMine) {
            field[posX][posY].setRevealed(true);
            isStopped = true;
            gameStatus = FAIL;
            return -1;
        }
        field[posX][posY].setRevealed(true);
        clearMinesCount--;
        if (field[posX][posY].getMinesNearby() == 0) {
            performActionForNeighbours(posX, posY, this::revealAt);
        }
        if (clearMinesCount == 0) {
            isStopped = true;
            gameStatus = VICTORY;
            return 1;
        }
        gameStatus = IN_PROCESS;
        return 0;
    }

    public int getClearMinesCount() {
        return clearMinesCount;
    }

    public void setFlag(int posX, int posY) {
        if (field[posX][posY].isRevealed()) {
            return;
        }
        if (field[posX][posY].isHasFlag()) {
            flags++;
            field[posX][posY].setHasFlag(false);
        } else {
            flags--;
            field[posX][posY].setHasFlag(true);
        }
    }

    public int getFlags() {
        return flags;
    }

    public void revealAll() {
        for (int i = 0; i < getWidth(); i++) {
            for (int j = 0; j < getHeight(); j++) {
                field[i][j].setRevealed(true);
            }
        }
    }

    public synchronized void setOnTimeUpdatedListener(OnTimeUpdatedListener listener) {
        this.onTimeUpdatedListener = listener;
    }

    public boolean isStopped() {
        return isStopped;
    }

    public interface OnTimeUpdatedListener {
        void performAction();
    }

    interface CellAction {
        void performAction(int x, int y);
    }
}
