import console_gui.ConsoleView;
import gui.GameFrame;
import model.Model;

import java.io.IOException;

public class Minesweeper {
    public static void main(String[] args) {
        if (args.length != 0 && args.length != 1) {
            System.out.println("Unknown keys");
            return;
        }
        Model model = new Model(Model.GameDifficulty.EASY);
        try {
            if (args.length == 0) {
                new ConsoleView(model).startGame();
            } else if (args[0].equals("-g") || args[0].equals("--gui")){
                new GameFrame(model).setVisible(true);
            } else {
                System.out.println("Unknown key");
            }
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Error on initialization factory");
            e.printStackTrace();
        }
    }
}
